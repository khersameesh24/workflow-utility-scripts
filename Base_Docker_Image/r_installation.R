# ==============================================================================
# Defines the R packages required
# ==============================================================================

# Install devtools to allow installation of versions for packages
install.packages("devtools", dependencies=TRUE, repos = "http://cran.us.r-project.org")

# Install specific versions of packages
require(devtools)
install_version("plyr", version="1.8.6")
install_version("optparse", version="1.6.6")
install_version("BiocManager", version="1.30.10")

# Install packages derived from BiocManager
BiocManager::install(version = '3.12')

# Include all other R packages here

# ==============================================================================
