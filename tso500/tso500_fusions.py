"""
Requirements : installed pandas package
python3 tso500_fusions.py <filename.vcf>
genes.tsv should be present on the path where script is run
Output of the program is a vcf file and a csv which is stored in :
[Folder] : /tso500_gene_fusions
[Folder] : /tmb_msi_tables
"""
import sys
import os
import csv
import ntpath
import shutil
import pandas as pd

gene_info = "genes.tsv"
# {{{ TMB
tmb = []
# }}}
# {{{ MSI
msi = []
# }}}
# {{{ Gene Fusions
fgenepair = []
fbreakpoint1 = []
fbreakpoint2 = []
# }}}


class SampleVcf:
    def __init__(self, tsv_input):
        self.tsv_input = tsv_input
        self.splicevariantsMap = {}
        self.fusionsMap = {}
        self.SECTIONS = {
            "[Analysis Details]": {"start": -1, "end": 0},
            "[Sequencing Run Details]": {"start": -1, "end": 0},
            "[TMB]": {"start": -1, "end": 0},
            "[MSI]": {"start": -1, "end": 0},
            "[Gene Amplifications]": {"start": -1, "end": 0},
            "[Splice Variants]": {"start": -1, "end": 0},
            "[Fusions]": {"start": -1, "end": 0},
            "[Small Variants]": {"start": -1, "end": 0},
        }

        self.parse(tsv_input)
        self.write_TMB_MSI_table(tsv_input)
        self.write_vcf(tsv_input)

    def parse(self, tsv_input):
        lines = []
        try:
            with open(tsv_input, newline="") as tsv_file:
                tsvReader = csv.reader(tsv_file, delimiter="\t", quotechar='"')
                lines = [line for line in tsvReader]
                self.get_SectionBoundary(lines)
                self.parse_Fusions(lines)
                self.parse_TMB(lines)
                self.parse_MSI(lines)
        except:
            print("[ERROR] Parsing input file", ntpath.basename(tsv_input))

    def get_SectionBoundary(self, lines):
        i = -1
        curSection = None
        for line in lines:
            i = i + 1
            if not line:
                continue
            if line[0] in self.SECTIONS.keys():
                curSection = line[0]
                self.SECTIONS[curSection]["start"] = i + 1
            else:
                if curSection:
                    self.SECTIONS[curSection]["end"] = i

    def parse_geneinfo(self):
        try:
            df = pd.read_csv(gene_info, sep="\t", index_col=False)
        except:
            print("[Error] Reading Gene Info File [genes.tsv]")
            print("[Error] Check if [genes.tsv] is on the same path as script! ")
        return df

    def parse_TMB(self, lines):
        start = self.SECTIONS["[TMB]"]["start"]
        end = self.SECTIONS["[TMB]"]["end"]
        for line in lines[start:end]:
            if not line:
                continue
            param = line[0]
            param_info = line[1]
            if not param:
                continue
            tmb.append(param_info)

    def parse_MSI(self, lines):
        start = self.SECTIONS["[MSI]"]["start"]
        end = self.SECTIONS["[MSI]"]["end"]
        for line in lines[start:end]:
            if not line:
                continue
            param = line[0]
            param_info = line[1]
            if not param:
                continue
            msi.append(param_info)

    def parse_Fusions(self, lines):
        start = self.SECTIONS["[Fusions]"]["start"]
        end = self.SECTIONS["[Fusions]"]["end"] + 1
        dataHeader = lines[start]
        for line in lines[start + 1 : end]:
            if not line:
                continue
            param = line[0]
            if not param:
                continue
            i = -1
            m = {}
            for token in line:
                i = i + 1
                if dataHeader[i] == "Gene Pair":
                    fgenepair.append(token)
                if dataHeader[i] == "Breakpoint 1":
                    fbreakpoint1.append(token)
                if dataHeader[i] == "Breakpoint 2":
                    fbreakpoint2.append(token)
                else:
                    m[dataHeader[i]] = token
            self.fusionsMap[param] = m

    def return_strand(self, genelist):
        df = self.parse_geneinfo()
        if (genelist) != ["NA"]:
            gene1_strand_df = df.loc[df["Symbol"] == genelist[0]]  # get strand
            gene1_strand = gene1_strand_df["Strand"].str.get(0)

            gene2_strand_df = df.loc[df["Symbol"] == genelist[1]]  # get strand
            gene2_strand = gene2_strand_df["Strand"].str.get(0)

            return (
                gene1_strand.to_string().split()[1],
                gene2_strand.to_string().split()[1],
            )

        else:
            print("[Error] Check if [Gene-Fusions] Exists in the input")

    def create_TMB_MSI_table(self, tsv_input):
        current_directory_path = os.getcwd()
        output_directory = "/tmb_msi_tables"
        path = current_directory_path + output_directory
        table_output = (
            str(ntpath.basename(tsv_input).split(".")[0]) + "_TMB_MSI_table.csv"
        )
        table_path = path + "/" + table_output
        if os.path.exists(path):
            if os.path.isfile(table_path):
                os.remove(table_path)
        else:
            os.mkdir(path)
        return table_path

    def write_TMB_MSI_table(self, tsv_input):
        table_path = self.create_TMB_MSI_table(tsv_input)
        with open(table_path, "w") as f:
            print(" [TMB] ", file=f)
            print("Total TMB                          ", "\t", tmb[0], file=f)
            print("Coding Region Size in Megabases    ", "\t", tmb[1], file=f)
            print("Number of Passing Eligible Variants", "\t", tmb[2], file=f)
            print("\n", file=f)
            print("\n [MSI] ", file=f)
            print("Usable MSI Sites                   ", "\t", msi[0], file=f)
            print("Total MSI Sites Unstable           ", "\t", msi[1], file=f)
            print("Percent Unstable MSI Sites         ", "\t", msi[2], file=f)
        print("\n[TMB]-[MSI] Tables Generated Successfully")

    def create_vcf(self, tsv_input):
        current_directory_path = os.getcwd()
        output_directory = "/tso500_gene_fusions"
        path = current_directory_path + output_directory
        vcf_output = str(ntpath.basename(tsv_input).split(".")[0]) + ".vcf"
        vcf_path = path + "/" + vcf_output
        if os.path.exists(path):
            if os.path.isfile(vcf_path):
                os.remove(vcf_path)
        else:
            os.mkdir(path)
        return vcf_path

    def write_vcf(self, tsv_input):
        self.create_vcf(tsv_input)
        strand_list = []
        df = pd.DataFrame(
            list(zip(fgenepair, fbreakpoint1, fbreakpoint2)),
            columns=["Gene Pairs", "Breakpoint1", "Breakpoint2"],
        )
        for genepair in df.iloc[:, 0]:
            strand_list.append(self.return_strand(genepair.split("-")))
        if strand_list != [None]:
            df["Strands"] = strand_list
            # Create column headers
            df_output_csv = pd.DataFrame(
                columns=[
                    "#CHROM",
                    "POS",
                    "ID",
                    "REF",
                    "ALT",
                    "QUAL",
                    "FILTER",
                    "INFO",
                    "FORMAT",
                    "SAMPLE",
                ]
            )
            # Create ALT column
            i = -1
            for _ in range(0, len(df)):
                i = i + 1
                if df["Strands"][i] == ("+", "-"):
                    ALT1 = "N" + "]" + df["Breakpoint2"][i] + "]"
                    ALT2 = "N" + "]" + df["Breakpoint1"][i] + "]"

                if df["Strands"][i] == ("-", "+"):
                    ALT1 = "[" + df["Breakpoint2"][i] + "[" + "N"
                    ALT2 = "[" + df["Breakpoint1"][i] + "[" + "N"

                if df["Strands"][i] == ("-", "-"):
                    ALT1 = "]" + df["Breakpoint2"][i] + "]" + "N"
                    ALT2 = "N" + "[" + df["Breakpoint1"][i] + "["

                if df["Strands"][i] == ("+", "+"):
                    ALT1 = "N" + "[" + df["Breakpoint2"][i] + "["
                    ALT2 = "]" + df["Breakpoint1"][i] + "]" + "N"

                # Create INFO column
                gene_pair = df["Gene Pairs"][i]
                concat_gene_pair = "SVTYPE=BND;" + gene_pair

                # Create CHROM and POS column
                CHROM1 = df["Breakpoint1"][i].split(":")[0]
                POS1 = df["Breakpoint1"][i].split(":")[1]

                CHROM2 = df["Breakpoint2"][i].split(":")[0]
                POS2 = df["Breakpoint2"][i].split(":")[1]
                # Create a df to print all values in the resp. columns
                df_output_csv = df_output_csv.append(
                    {
                        "#CHROM": CHROM1,
                        "POS": POS1,
                        "ID": "1_1",
                        "REF": "N",
                        "ALT": ALT1,
                        "QUAL": ".",
                        "FILTER": ".",
                        "INFO": concat_gene_pair,
                        "FORMAT": "GT",
                        "SAMPLE": "0/1",
                    },
                    ignore_index=True,
                    sort=False,
                )
                df_output_csv = df_output_csv.append(
                    {
                        "#CHROM": CHROM2,
                        "POS": POS2,
                        "ID": "1_2",
                        "REF": "N",
                        "ALT": ALT2,
                        "QUAL": ".",
                        "FILTER": ".",
                        "INFO": concat_gene_pair,
                        "FORMAT": "GT",
                        "SAMPLE": "0/1",
                    },
                    ignore_index=True,
                    sort=False,
                )

        else:
            print("[Error] Check if [Gene Fusions] Exists in input tsv!")

        vcf_path = self.create_vcf(tsv_input)
        with open(vcf_path, "w", newline="") as f:
            print(
                "##fileformat=VCFv4.2",
                "\n##reference=hg19",
                '\n##INFO=<ID=SVTYPE=BND,Number=1,Type=String,Description="Type of structural variant">',
                '\n##INFO=<ID=JSCT,Number=1,Type=String,Description="JunctionReads: Junction Spanning Reads">',
                '\n##INFO=<ID=FFPM,Number=1,Type=Float,Description="Fusion fragments per million total reads">',
                '\n##INFO=<ID=ENSGID,Number=1,Type=String,Description="Ensembl Gene ID">',
                '\n##INFO=<ID=SVCLASS,Number=1,Type=String,Description="Class of genomic translocation">',
                '\n##INFO=<ID=RN,Number=1,Type=Integer,Description="Denotes an RNA Variant">',
                '\n##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">',
                file=f,
            )
        f.close()
        if strand_list != [None]:
            df_output_csv.to_csv(vcf_path, mode="a", sep="\t", index=False)
            print("\n[vcf] Generated Successfully!\n")
        else:
            with open(vcf_path, "a") as f:
                print(
                    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tSAMPLE",
                    file=f,
                )
            print("\n[vcf] Generated with No [Gene Fusion] Events")


if __name__ == "__main__":
    tsv_input = sys.argv[1]
    s = SampleVcf(tsv_input)
